import React from 'react';
import { Link } from 'react-router-dom'
import ProLayout, {
    MenuDataItem,
    BasicLayoutProps as ProLayoutProps,
    Settings,
  } from '@ant-design/pro-layout';
import {Layout} from 'antd';

interface IProps {
    logo: any;
    menuData: MenuDataItem[];
}

/* Navigation Bar components */
const Navbar: React.FC<IProps>= (props) => {

    const footerRender = (): React.ReactNode => {
        return (
          <Layout style={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems:'center'
          }}>
            <p>Author: Pierre-Alexandre</p>
          </Layout>
          );
      }

    return (
        <React.Fragment>
                <ProLayout
                  title='Kicklox'
                  logo={props.logo}
                  menuDataRender={() => props.menuData}
                  menuItemRender={(menuItemProps, defaultDom) => (
                   <Link to={menuItemProps.path}>{defaultDom}</Link>
                  )}
                  pageTitleRender={() => { return ("Kicklox Chat")}}
                  footerRender={footerRender}
                >
                  {props.children}
                </ProLayout>
        </React.Fragment>
    );
}

export default Navbar