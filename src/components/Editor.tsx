import React from 'react';
import {
    Input, 
    Button, 
    Form,
    Switch,
  } from 'antd';

const { TextArea } = Input;

/*Editor component for comments */
const Editor = ({ onChange, onSubmit, submitting, value, onPrivate }: any) => (
    <div style={{
      width:'80%'
    }}>
      <Form.Item>
        <TextArea rows={4} onChange={onChange} value={value} />
      </Form.Item>
      <Form.Item>
        <Switch defaultChecked checkedChildren='Private' unCheckedChildren='Public' onChange={onPrivate} />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
          Add Comment
        </Button>
      </Form.Item>
    </div>
  );
  
export default Editor;