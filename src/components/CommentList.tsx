import React from 'react';
import {
    List,
    Comment,
  } from 'antd';

/* List of discussion general */
const CommentList = ({ comments }: any) => (
    <List
      pagination={{
        onChange: page => {
        },
        pageSize: 5,
      }}
      dataSource={comments}
      header={`${comments.length} ${comments.length > 1 ? 'replies' : 'reply'}`}
      itemLayout="horizontal"
      renderItem={(props: any) => <Comment {...props} />}
    />
);

export default CommentList;