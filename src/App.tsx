import React from 'react';
import './App.css';
import {
  Comment,
  Avatar,
  Row,
  Col,
  Layout
} from 'antd';
import moment from 'moment';
import {
  Navbar,
  Editor,
  CommentList
} from './components';
import {LogoJPG} from './assets';


interface IProps {
}

const App: React.FC<IProps> = (props) => {
  const [comments, setComments] = React.useState<Array<any>>([]);
  const [privComments, setPrivComments] = React.useState<Array<any>>([]);
  const [submitting, setSubmitting] = React.useState<boolean>(false);
  const [value, setValue] = React.useState<string>("");
  const [priv, setPriv] = React.useState<boolean>(true)
 

  const handleSubmit = () => {
    if (!value) {
      return;
    }
    setSubmitting(true);

    setTimeout(() => {
      setSubmitting(false);
      setValue('');
      if (!priv) {
      setComments([{
        author: 'Kicklox user',
        avatar: LogoJPG,
        content: <p>{value}</p>,
        datetime: moment().fromNow(),
      }, ...comments])
      }else {
        setPrivComments([{
          author: 'Kicklox user',
          avatar: LogoJPG,
          content: <p>{value}</p>,
          datetime: moment().fromNow(),
        }, ...privComments])
      }
    }, 1000);
  };

  const handleChange = (e: any) => {
    setValue(e.target.value);
  };

  const handlePrivate = (checked: any) => {
    setPriv(checked);
  }

  return (
    <Navbar
      logo={LogoJPG}
      menuData={[]}
    >
        <Row>
          <Col span={12} style={{
            paddingRight: '1rem'
          }}>
            <h1 style={{
              textAlign: 'center'
            }}>Public Discussion</h1>
            {comments.length > 0 && <CommentList comments={comments} priv={priv} />}
          </Col>
          <Col span={12}>
            <h1 style={{
              textAlign: 'center'
            }}>Private Discussion</h1>
            {privComments.length > 0 && <CommentList comments={privComments} />}
          </Col>
        </Row>
        <Layout style={{
          position: 'fixed',
          bottom: '0',
          width: '100%',
        }}>
        <Comment
          avatar={
            <Avatar
              src={LogoJPG}
              alt="Kicklox user"
            />
          }
          content={
            <Editor
              onChange={handleChange}
              onSubmit={handleSubmit}
              submitting={submitting}
              value={value}
              onPrivate={handlePrivate}
            />
          }
          
        />
      </Layout>
    </Navbar>
  );
}

export default App;
