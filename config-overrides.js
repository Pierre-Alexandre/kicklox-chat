const {
  override,
  fixBabelImports,
  addLessLoader,
} = require("customize-cra");


module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    // Customize variables
    // https://ant.design/docs/react/customize-theme
    modifyVars: { "@primary-color": "#34AE9D" }
  })
);
